package com.cgi.aggregationservice.soap;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class TicketConfiguration {

    @Value("${Jira.url}")
    private String Jira;
    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.cgi.aggregationservice.wsdl");
        return marshaller;
    }

    @Bean
    public TicketClient ticketClient(Jaxb2Marshaller marshaller) {
        TicketClient ticket = new TicketClient();
        ticket.setDefaultUri(Jira);
        ticket.setMarshaller(marshaller);
        ticket.setUnmarshaller(marshaller);
        return ticket;
    }

}
