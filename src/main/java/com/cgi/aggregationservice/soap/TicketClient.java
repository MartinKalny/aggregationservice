package com.cgi.aggregationservice.soap;

import com.cgi.aggregationservice.wsdl.GetTicketRequest;
import com.cgi.aggregationservice.wsdl.GetTicketResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class TicketClient extends WebServiceGatewaySupport {

    @Value("${Jira.url}")
    private String Jira;

    public GetTicketResponse getTicket(Long id) {

            GetTicketRequest request = new GetTicketRequest();
            request.setId(id);

            GetTicketResponse response = (GetTicketResponse) getWebServiceTemplate()
                    .marshalSendAndReceive(Jira, request);

            return response;
        }

    }
