package com.cgi.aggregationservice.rest.exceptionhandling;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ApiError {
    private LocalDateTime timestamp;
    private int statusCode;
    private String message;
    private List<String> errors = new ArrayList<>();
    private String path;

    public ApiError() {
    }

    public ApiError(int statusCode, String message, List<String> errors, String path) {
        this.timestamp = LocalDateTime.now(Clock.systemUTC());
        this.statusCode = statusCode;
        this.message = message;
        this.errors = errors;
        this.path = path;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
    public void setErrors(String errors) {
        this.errors = Arrays.asList(errors);
    }
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "ApiError{" +
                "timestamp=" + timestamp +
                ", statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", errors=" + errors +
                ", path='" + path + '\'' +
                '}';
    }
}
