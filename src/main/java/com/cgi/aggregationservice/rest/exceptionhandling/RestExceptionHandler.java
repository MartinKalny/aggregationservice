package com.cgi.aggregationservice.rest.exceptionhandling;

import com.cgi.aggregationservice.service.exceptions.ResourceNotFoundException;
import com.cgi.aggregationservice.service.exceptions.UnauthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletRequest;


@RestControllerAdvice
public class RestExceptionHandler {

    private static final UrlPathHelper URL_PATH_HELPER = new UrlPathHelper();

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ApiError> resourceNotFoundException(ResourceNotFoundException resourceNotFound,
                                                              final HttpServletRequest request) {
        ApiError apiError = new ApiError(404,
                resourceNotFound.getLocalizedMessage(),
                null,
                URL_PATH_HELPER.getOriginatingServletPath(request));
        apiError.setErrors("Resource not found.");
        return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<ApiError> unauthorizedException(UnauthorizedException ex,
                                                          final HttpServletRequest request){
        ApiError apiError = new ApiError(401,
                ex.getLocalizedMessage(),
                null,
                URL_PATH_HELPER.getOriginatingServletPath(request));
        apiError.setErrors("Unauthorized.");
        return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> handleAll(Exception ex, final HttpServletRequest request) {
        ApiError apiError = new ApiError(500,
                ex.getLocalizedMessage(),
                null,
                URL_PATH_HELPER.getOriginatingServletPath(request));
        return new ResponseEntity<>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
