package com.cgi.aggregationservice.rest;

import com.cgi.aggregationservice.persistence.entitites.*;
import com.cgi.aggregationservice.service.messaging.JmsListenerService;
import com.cgi.aggregationservice.service.TicketService;
import com.cgi.aggregationservice.wsdl.TicketSoap;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/tickets")
public class RestAPI {

    private TicketService ticketService;
    private JmsListenerService jmsListenerService;

    @Autowired
    public RestAPI(TicketService ticketService, JmsListenerService jmsListenerService) {
        this.ticketService = ticketService;
        this.jmsListenerService = jmsListenerService;
    }

    @GetMapping(path = "/messages")
    @ApiOperation(value = "Gets List of newly created tickets in ServiceNow",
            response = TicketJmsDto.class,
            responseContainer = "List")
    public List<TicketJmsDto> getAmqMessages(){
        return jmsListenerService.getAmqList();
    }

    @GetMapping(path = "/messages/{id}")
    @ApiOperation(value = "Gets info about newly created ticket in ServiceNow by id",
            notes = "Provide an id to find specific ticket",
            response = TicketJmsDto.class)
    public TicketJmsDto getAmqMessageById(@PathVariable Long id){
        return jmsListenerService.getMessage(id);
    }

    @GetMapping(path = "/service-now/{id}")
    @ApiOperation(value = "Gets Ticket from ServiceNow by id",
            notes = "Provide an id to find specific ticket",
            response = Ticket.class)
    public ResponseEntity<Ticket> getTicketById(@PathVariable Long id) {
        return ticketService.getServiceNowTicket(id);
    }

    @GetMapping(path = "/service-now")
    @ApiOperation(value = "Gets all Tickets from ServiceNow",
            notes = "Provide page and number of results per page",
            response = Ticket.class)
    public PageResultResource<Ticket> getAllTickets(@RequestParam(defaultValue = "1", name = "page") int page,
                                                    @RequestParam(defaultValue = "5", name = "size") int size){

       return ticketService.getAllServiceNowTickets(page, size);

    }

    @GetMapping(path = "/service-now/{id}/info")
    @ApiOperation(value = "Gets Tickets from ServiceNow and information about its creator",
            notes = "Provide an id to find specific ticket. User needs to have manager role.",
            response = TicketInfo.class)
    public ResponseEntity<Object> getTicketInfoById(@PathVariable Long id,
                                                    @RequestHeader(value = "authorization") String authorization) {
                return ticketService.getServiceNowTicketInfo(id, authorization);
    }

    @GetMapping(path = "/jira/{id}")
    @ApiOperation(value = "Gets Tickets from Jira by id",
            notes = "Provide an id to find specific ticket",
            response = TicketSoap.class)
    public TicketSoap getJiraTicketById(@PathVariable Long id) {
        return ticketService.getJiraTicket(id);
    }

    @GetMapping(path = "/jira/{id}/info")
    @ApiOperation(value = "Gets Tickets from Jira and information about its creator",
            notes = "Provide an id to find specific ticket",
            response = JiraTicketInfo.class)
    public ResponseEntity<Object> getJiraTicketInfoById(@PathVariable Long id,
                                                        @RequestHeader(value = "authorization") String authorization) {
        return ticketService.getJiraTicketInfo(id, authorization);
    }
}
