package com.cgi.aggregationservice.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    // http://localhost:8085/swagger-ui.html
    // http://localhost:8085/v2/api-docs
    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.ant("/tickets/**"))
                .apis(RequestHandlerSelectors.basePackage("com.cgi.aggregationservice.rest"))
                .build()
                .apiInfo(apiDetails());
    }

    private ApiInfo apiDetails(){
        return new ApiInfo(
                "Aggregation Service API",
                "API documentation for aggregation service",
                "1.0",
                "Terms of Service",
                "Martin Kalny",
                "Apache 2.0",
                "https://www.apache.org/licenses/"
        );
    }
}
