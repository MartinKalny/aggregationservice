package com.cgi.aggregationservice.service;

import com.cgi.aggregationservice.persistence.entitites.*;
import com.cgi.aggregationservice.service.exceptions.ResourceNotFoundException;
import com.cgi.aggregationservice.service.exceptions.UnauthorizedException;
import com.cgi.aggregationservice.soap.TicketClient;
import com.cgi.aggregationservice.wsdl.TicketSoap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Service
public class TicketService {
    private RestTemplate restTemplate;
    private TicketClient ticketClient;

    @Value("${ServiceNow.url}")
    private String ServiceNow;

    @Value("${UserManagement.url}")
    private String UserManagement;

    @Autowired
    public TicketService(RestTemplate restTemplate, TicketClient ticketClient) {
        this.restTemplate = restTemplate;
        this.ticketClient = ticketClient;
    }

    public ResponseEntity<Ticket> getServiceNowTicket(Long id) {
        try {
            String url = ServiceNow + "/" + id;
            return restTemplate.getForEntity(url, Ticket.class);
        } catch (HttpClientErrorException ex) {
            throw new ResourceNotFoundException("Resource with id = " + id + " was not found");
        }
    }

    public ResponseEntity<Object> getServiceNowTicketInfo(Long id, String authorization) {
        try {
            TicketInfo ticketInfo = new TicketInfo();

            String url = ServiceNow + "/" + id;
            Ticket ticket = restTemplate.getForObject(url, Ticket.class);
            ticketInfo.setTicket(ticket);
            String urlPerson = UserManagement + "/" + ticket.getIdPersonCreator();

            String[] credentials = authorize(authorization);

            ResponseEntity<PersonBasic> personBasic = restTemplate.exchange(urlPerson, HttpMethod.GET,
                    new HttpEntity<>(createHttpHeaders(credentials[0], credentials[1])), PersonBasic.class);

            ticketInfo.setPersonBasic(personBasic.getBody());
            return new ResponseEntity<>(ticketInfo, HttpStatus.OK);
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode().value() == 404) {
                throw new ResourceNotFoundException("Resource with id = " + id + " was not found");
            } else if (ex.getStatusCode().value() == 401) {
                throw new UnauthorizedException("Not authorized");
            } else {
                throw ex;
            }
        }
    }

    public PageResultResource<Ticket> getAllServiceNowTickets(int page, int size) {
        String url = ServiceNow + "?page=" + page + "&size=" + size;

        ResponseEntity<PageResultResource<Ticket>> ticketResponse =
                restTemplate.exchange(url, HttpMethod.GET, null,
                        new ParameterizedTypeReference<PageResultResource<Ticket>>() {
                        });
        PageResultResource<Ticket> rates = ticketResponse.getBody();
        return rates;
    }

    public TicketSoap getJiraTicket(Long id) {
        try {
        return ticketClient.getTicket(id).getTicketSoap();
        } catch (SoapFaultClientException ex) {
            throw new ResourceNotFoundException("Resource with id = " + id + " was not found");
        }
    }

    public ResponseEntity<Object> getJiraTicketInfo(Long id, String authorization) {
        try {
        JiraTicketInfo jiraTicketInfo = new JiraTicketInfo();
        TicketSoap ticketResponse = ticketClient.getTicket(id).getTicketSoap();
        jiraTicketInfo.setTicketById(ticketResponse);
        String urlPerson = UserManagement + "/" + id;
        String[] credentials = authorize(authorization);
            ResponseEntity<PersonBasic> personBasic = restTemplate.exchange(urlPerson, HttpMethod.GET,
                    new HttpEntity<>(createHttpHeaders(credentials[0], credentials[1])), PersonBasic.class);
            jiraTicketInfo.setPersonBasic(personBasic.getBody());
            return new ResponseEntity<>(jiraTicketInfo, HttpStatus.OK);
        } catch (SoapFaultClientException ex) {
                throw new ResourceNotFoundException("Resource with id = " + id + " was not found");
        } catch (HttpClientErrorException ex){
            throw new UnauthorizedException("Not authorized");
        }
    }

    private HttpHeaders createHttpHeaders(String user, String password) {
        String notEncoded = user + ":" + password;
        String encodedAuth = "Basic " + Base64.getEncoder().encodeToString(notEncoded.getBytes());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", encodedAuth);
        return headers;
    }


    public String[] authorize(String authorization) {
        if (authorization != null && authorization.toLowerCase().startsWith("basic")) {
            String base64Credentials = authorization.substring("Basic".length()).trim();
            byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
            String credentials = new String(credDecoded, StandardCharsets.UTF_8);
            // credentials = username:password
            return credentials.split(":", 2);
        } else {
            return null;
        }
    }
}
