package com.cgi.aggregationservice.service.messaging;

import com.cgi.aggregationservice.persistence.entitites.AmqMessages;
import com.cgi.aggregationservice.persistence.entitites.TicketJmsDto;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class JmsListenerService {

    @JmsListener(destination = "tickets.queue")
    public void receiveMessage(TicketJmsDto msg){
        System.out.println("Received <" + msg + ">");
        if(msg != null){
            getAmqList().add(msg);
        }
    }

    public List<TicketJmsDto> getAmqList() {
        return AmqMessages.getInstance().getArrayList();
    }

    public TicketJmsDto getMessage(Long id){
        return getAmqList().stream()
                .filter(t -> t.getId().equals(id))
                .findAny()
                .orElse(null);
    }
}
