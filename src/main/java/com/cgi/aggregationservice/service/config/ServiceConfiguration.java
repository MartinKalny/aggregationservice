package com.cgi.aggregationservice.service.config;

import com.cgi.aggregationservice.persistence.entitites.TicketJmsDto;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class ServiceConfiguration {

    @Bean
    public RestTemplate restTemplate() throws Exception {
        SSLContext sslContext = new SSLContextBuilder()
                .loadTrustMaterial(ResourceUtils.getFile("src/main/resources/test.p12"), "password".toCharArray())
                .build();

        SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext);

        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(socketFactory)
                .build();

        HttpComponentsClientHttpRequestFactory factory =
                new HttpComponentsClientHttpRequestFactory(httpClient);
        return new RestTemplate(factory);
    }



    @Bean
    public MappingJackson2MessageConverter messageConverter() {
        MappingJackson2MessageConverter messageConverter = new MappingJackson2MessageConverter();
        messageConverter.setTypeIdPropertyName("_typeId"); // by default fully classified name of the Class

        setTypeMappings(messageConverter);
        return messageConverter;
    }

    private void setTypeMappings(MappingJackson2MessageConverter messageConverter) {
        Map<String, Class<?>> typeIdMappings = new HashMap<>();
        typeIdMappings.put("ticket", TicketJmsDto.class);
        messageConverter.setTypeIdMappings(typeIdMappings);
    }
}
