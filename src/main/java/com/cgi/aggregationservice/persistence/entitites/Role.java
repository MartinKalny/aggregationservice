package com.cgi.aggregationservice.persistence.entitites;

public class Role {
    private String type;

    public Role() {
    }

    public Role(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Role{" +
                "role_type='" + type + '\'' +
                '}';
    }
}
