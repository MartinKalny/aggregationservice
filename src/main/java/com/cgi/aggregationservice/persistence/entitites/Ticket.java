package com.cgi.aggregationservice.persistence.entitites;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@ApiModel(description = "Details about a ticket")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)  //global setting doesnt work for deserialization
public class Ticket {
    @ApiModelProperty(notes = "The unique id of a person")
    private Long id;
    @ApiModelProperty(notes = "The person's name")
    private String name;
    @ApiModelProperty(notes = "The unique id of the person to whom the ticket was assigned")
    private Long idPersonAssigned;
    @ApiModelProperty(notes = "The unique id of the ticket's creator")
    private Long idPersonCreator;
    @ApiModelProperty(notes = "The time and date when the ticket was created")
    private LocalDateTime creationDatetime;

    public Ticket() {
    }

    public Ticket(Long id, String name, Long idPersonAssigned, Long idPersonCreator, LocalDateTime creationDatetime) {
        this.id = id;
        this.name = name;
        this.idPersonAssigned = idPersonAssigned;
        this.idPersonCreator = idPersonCreator;
        this.creationDatetime = creationDatetime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public LocalDateTime getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(LocalDateTime creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", creationDatetime=" + creationDatetime +
                '}';
    }
}
