package com.cgi.aggregationservice.persistence.entitites;


import com.cgi.aggregationservice.wsdl.TicketSoap;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Details about a ticket from Jira")
public class JiraTicketInfo {
    @ApiModelProperty(notes = "The ticket information")
    private TicketSoap ticketById;
    @ApiModelProperty(notes = "The creator's information")
    private PersonBasic personBasic;

    public JiraTicketInfo() {
    }

    public JiraTicketInfo(TicketSoap ticketById, PersonBasic personBasic) {
        this.ticketById = ticketById;
        this.personBasic = personBasic;
    }

    public TicketSoap getTicketById() {
        return ticketById;
    }

    public void setTicketById(TicketSoap ticketById) {
        this.ticketById = ticketById;
    }

    public PersonBasic getPersonBasic() {
        return personBasic;
    }

    public void setPersonBasic(PersonBasic personBasic) {
        this.personBasic = personBasic;
    }

    @Override
    public String toString() {
        return "JiraTicketInfo{" +
                "ticketById=" + ticketById +
                ", personBasic=" + personBasic +
                '}';
    }
}
