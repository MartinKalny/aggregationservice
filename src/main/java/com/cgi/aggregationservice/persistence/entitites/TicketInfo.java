package com.cgi.aggregationservice.persistence.entitites;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Details about the a from ServiceNow")
public class TicketInfo {
    @ApiModelProperty(notes = "The ticket information")
    private Ticket ticket;
    @ApiModelProperty(notes = "The creator's information")
    private PersonBasic personBasic;

    public TicketInfo() {
    }

    public TicketInfo(Ticket ticket, PersonBasic personBasic) {
        this.ticket = ticket;
        this.personBasic = personBasic;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public PersonBasic getPersonBasic() {
        return personBasic;
    }

    public void setPersonBasic(PersonBasic personBasic) {
        this.personBasic = personBasic;
    }

    @Override
    public String toString() {
        return "TicketInfo{" +
                "ticket=" + ticket +
                ", personBasic=" + personBasic +
                '}';
    }
}
