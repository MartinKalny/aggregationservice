package com.cgi.aggregationservice.persistence.entitites;

import java.util.ArrayList;

public class AmqMessages {

        private ArrayList<TicketJmsDto> messageList;

        private static AmqMessages instance;

        private AmqMessages(){
            messageList = new ArrayList<TicketJmsDto>();
        }

        public static AmqMessages getInstance(){
            if (instance == null){
                instance = new AmqMessages();
            }
            return instance;
        }

        public ArrayList<TicketJmsDto> getArrayList() {
            return messageList;
        }
}


